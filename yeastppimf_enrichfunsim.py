import sys
sys.path.append('/mnt/projects/liuwt/pathway/EnrichFunSim')
import EnrichFunSim
from EnrichFunSim import *
from EnrichFunSim.ProteinSimilarityModify import *

enrichfuncsim('tests/nsgdMF.txt', Targets ='tests/yeastppinewmf.txt', ontology = 'MF', measure = ('ravg', 'rabm', 'rbma', 'rmax'), output=0, outputfile='finalresults/rmodifyMFyeastppinew.txt') 
enrichfuncsim('tests/nsgdMF.txt', Targets ='tests/yeastppinewmf.txt', ontology = 'MF', measure = ('xravg', 'xrabm', 'xrbma', 'xrmax'), output=0, outputfile='finalresults/xrmodifyMFyeastppinew.txt')
enrichfuncsim('tests/nsgdMF.txt', Targets ='tests/yeastppinewmf.txt', ontology = 'MF', measure = ('navg', 'nabm', 'nbma', 'nmax'), output=0, outputfile='finalresults/nmodifyMFyeastppinew.txt')
enrichfuncsim('tests/nsgdMF.txt', Targets ='tests/yeastppinewmf.txt', ontology = 'MF', measure = ('xnavg', 'xnabm', 'xnbma', 'xnmax'), output=0, outputfile='finalresults/xnmodifyMFyeastppinew.txt')
enrichfuncsim('tests/nsgdMF.txt', Targets ='tests/yeastppinewmf.txt', ontology = 'MF', measure = ('lavg', 'labm', 'lbma', 'lmax'), output=0, outputfile='finalresults/lmodifyMFyeastppinew.txt')
enrichfuncsim('tests/nsgdMF.txt', Targets ='tests/yeastppinewmf.txt', ontology = 'MF', measure = ('xlavg', 'xlabm', 'xlbma', 'xlmax'), output=0, outputfile='finalresults/xlmodifyMFyeastppinew.txt')
enrichfuncsim('tests/nsgdMF.txt', Targets ='tests/yeastppinewmf.txt', ontology = 'MF', measure = ('savg', 'sabm', 'sbma', 'smax'), output=0, outputfile='finalresults/smodifyMFyeastppinew.txt')
enrichfuncsim('tests/nsgdMF.txt', Targets ='tests/yeastppinewmf.txt', ontology = 'MF', measure = ('agic', 'adic', 'auic'), output=0, outputfile='finalresults/amodifyMFyeastppinew.txt')
enrichfuncsim('tests/nsgdMF.txt', Targets ='tests/yeastppinewmf.txt', ontology = 'MF', measure = ('zavg', 'zabm', 'zbma', 'zmax'), output=0, outputfile='finalresults/zmodifyMFyeastppinew.txt')
enrichfuncsim('tests/nsgdMF.txt', Targets ='tests/yeastppinewmf.txt', ontology = 'MF', measure = ('zgic', 'zdic', 'zuic'), output=0, outputfile='finalresults/zamodifyMFyeastppinew.txt')
enrichfuncsim('tests/nsgdMF.txt', Targets ='tests/yeastppinewmf.txt', ontology = 'MF', measure = ('wavg', 'wabm', 'wbma', 'wmax'), output=0, outputfile='finalresults/wmodifyMFyeastppinew.txt')
enrichfuncsim('tests/nsgdMF.txt', Targets ='tests/yeastppinewmf.txt', ontology = 'MF', measure = ('wgic', 'wdic', 'wuic'), output=0, outputfile='finalresults/wamodifyMFyeastppinew.txt')
enrichfuncsim('tests/nsgdMF.txt', Targets ='tests/yeastppinewmf.txt', ontology = 'MF', measure = ('uavg', 'uabm', 'ubma', 'umax'), output=0, outputfile='finalresults/umodifyMFyeastppinew.txt')
enrichfuncsim('tests/nsgdMF.txt', Targets ='tests/yeastppinewmf.txt', ontology = 'MF', measure = ('ugic', 'udic', 'uuic', 'ui'), output=0, outputfile='finalresults/uamodifyMFyeastppinew.txt')

from EnrichFunSim.ProteinSimilaritytwModify import *
enrichfuncsim('tests/nsgdMF.txt', Targets ='tests/yeastppinewmf.txt', ontology = 'MF', measure = ('teng', 'wis'), output=0, outputfile='finalresults/twmodifyMFyeastppinew.txt')

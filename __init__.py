__all__ = ['funcsim', 'enrichfuncsim']
__version__ = '1.1'
__release__ = True
__author__ = """Liu Wenting (liuwt@gis.a-star.edu.sg)\n(c) 2017 All rights reserved."""


from EnrichFunSim import *
#from EnrichFunSim.ProteinSimilarity import funcsim
#from EnrichFunSim.ProteinSimilarityModify import enrichfuncsim



import sys
sys.path.append('/mnt/projects/liuwt/pathway/EnrichFunSim')
import EnrichFunSim
from EnrichFunSim import *
from EnrichFunSim.ProteinSimilarity import *

enrichfuncsim('tests/nsgdMF.txt', Targets ='tests/yeastppinewmf.txt', ontology = 'MF', measure = ('ravg', 'rabm', 'rbma', 'rmax'), output=0, outputfile='finalresults/rMFyeastppinew.txt') 
enrichfuncsim('tests/nsgdMF.txt', Targets ='tests/yeastppinewmf.txt', ontology = 'MF', measure = ('xravg', 'xrabm', 'xrbma', 'xrmax'), output=0, outputfile='finalresults/xrMFyeastppinew.txt')
enrichfuncsim('tests/nsgdMF.txt', Targets ='tests/yeastppinewmf.txt', ontology = 'MF', measure = ('navg', 'nabm', 'nbma', 'nmax'), output=0, outputfile='finalresults/nMFyeastppinew.txt')
enrichfuncsim('tests/nsgdMF.txt', Targets ='tests/yeastppinewmf.txt', ontology = 'MF', measure = ('xnavg', 'xnabm', 'xnbma', 'xnmax'), output=0, outputfile='finalresults/xnMFyeastppinew.txt')
enrichfuncsim('tests/nsgdMF.txt', Targets ='tests/yeastppinewmf.txt', ontology = 'MF', measure = ('lavg', 'labm', 'lbma', 'lmax'), output=0, outputfile='finalresults/lMFyeastppinew.txt')
enrichfuncsim('tests/nsgdMF.txt', Targets ='tests/yeastppinewmf.txt', ontology = 'MF', measure = ('xlavg', 'xlabm', 'xlbma', 'xlmax'), output=0, outputfile='finalresults/xlMFyeastppinew.txt')
enrichfuncsim('tests/nsgdMF.txt', Targets ='tests/yeastppinewmf.txt', ontology = 'MF', measure = ('savg', 'sabm', 'sbma', 'smax'), output=0, outputfile='finalresults/sMFyeastppinew.txt')
enrichfuncsim('tests/nsgdMF.txt', Targets ='tests/yeastppinewmf.txt', ontology = 'MF', measure = ('agic', 'adic', 'auic'), output=0, outputfile='finalresults/aMFyeastppinew.txt')
enrichfuncsim('tests/nsgdMF.txt', Targets ='tests/yeastppinewmf.txt', ontology = 'MF', measure = ('zavg', 'zabm', 'zbma', 'zmax'), output=0, outputfile='finalresults/zMFyeastppinew.txt')
enrichfuncsim('tests/nsgdMF.txt', Targets ='tests/yeastppinewmf.txt', ontology = 'MF', measure = ('zgic', 'zdic', 'zuic'), output=0, outputfile='finalresults/zaMFyeastppinew.txt')
enrichfuncsim('tests/nsgdMF.txt', Targets ='tests/yeastppinewmf.txt', ontology = 'MF', measure = ('wavg', 'wabm', 'wbma', 'wmax'), output=0, outputfile='finalresults/wMFyeastppinew.txt')
enrichfuncsim('tests/nsgdMF.txt', Targets ='tests/yeastppinewmf.txt', ontology = 'MF', measure = ('wgic', 'wdic', 'wuic'), output=0, outputfile='finalresults/waMFyeastppinew.txt')
enrichfuncsim('tests/nsgdMF.txt', Targets ='tests/yeastppinewmf.txt', ontology = 'MF', measure = ('uavg', 'uabm', 'ubma', 'umax'), output=0, outputfile='finalresults/uMFyeastppinew.txt')
enrichfuncsim('tests/nsgdMF.txt', Targets ='tests/yeastppinewmf.txt', ontology = 'MF', measure = ('ugic', 'udic', 'uuic', 'ui'), output=0, outputfile='finalresults/uaMFyeastppinew.txt')

from EnrichFunSim.ProteinSimilaritytw import *
enrichfuncsim('tests/nsgdMF.txt', Targets ='tests/yeastppinewmf.txt', ontology = 'MF', measure = ('teng', 'wis'), output=0, outputfile='finalresults/twMFyeastppinew.txt')

To use the package, please cite
Liu, W., Liu, J., & Rajapakse, J. C. (2018). Gene Ontology Enrichment Improves Performances of Functional Similarity of Genes. Scientific Reports, 8(1), 12100. https://doi.org/10.1038/s41598-018-30455-0

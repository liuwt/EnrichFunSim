
import sys
sys.path.append('/mnt/projects/liuwt/pathway/EnrichFunSim')
import EnrichFunSim
from EnrichFunSim import *
from EnrichFunSim.ProteinSimilarity import *

############################## funsim with GO-enrichment on PPIN datasets with BP, MF, CC 
enrichfuncsim('tests/nsgdBP.txt', Targets ='tests/yeastppinewbp.txt', ontology = 'BP', measure = ('ravg', 'rabm', 'rbma', 'rmax'), output=0, outputfile='finalresults/rBPyeastppinew.txt') 
enrichfuncsim('tests/nsgdBP.txt', Targets ='tests/yeastppinewbp.txt', ontology = 'BP', measure = ('xravg', 'xrabm', 'xrbma', 'xrmax'), output=0, outputfile='finalresults/xrBPyeastppinew.txt')
enrichfuncsim('tests/nsgdBP.txt', Targets ='tests/yeastppinewbp.txt', ontology = 'BP', measure = ('navg', 'nabm', 'nbma', 'nmax'), output=0, outputfile='finalresults/nBPyeastppinew.txt')
enrichfuncsim('tests/nsgdBP.txt', Targets ='tests/yeastppinewbp.txt', ontology = 'BP', measure = ('xnavg', 'xnabm', 'xnbma', 'xnmax'), output=0, outputfile='finalresults/xnBPyeastppinew.txt')
enrichfuncsim('tests/nsgdBP.txt', Targets ='tests/yeastppinewbp.txt', ontology = 'BP', measure = ('lavg', 'labm', 'lbma', 'lmax'), output=0, outputfile='finalresults/lBPyeastppinew.txt')
enrichfuncsim('tests/nsgdBP.txt', Targets ='tests/yeastppinewbp.txt', ontology = 'BP', measure = ('xlavg', 'xlabm', 'xlbma', 'xlmax'), output=0, outputfile='finalresults/xlBPyeastppinew.txt')
enrichfuncsim('tests/nsgdBP.txt', Targets ='tests/yeastppinewbp.txt', ontology = 'BP', measure = ('savg', 'sabm', 'sbma', 'smax'), output=0, outputfile='finalresults/sBPyeastppinew.txt')
enrichfuncsim('tests/nsgdBP.txt', Targets ='tests/yeastppinewbp.txt', ontology = 'BP', measure = ('agic', 'adic', 'auic'), output=0, outputfile='finalresults/aBPyeastppinew.txt')
enrichfuncsim('tests/nsgdBP.txt', Targets ='tests/yeastppinewbp.txt', ontology = 'BP', measure = ('zavg', 'zabm', 'zbma', 'zmax'), output=0, outputfile='finalresults/zBPyeastppinew.txt')
enrichfuncsim('tests/nsgdBP.txt', Targets ='tests/yeastppinewbp.txt', ontology = 'BP', measure = ('zgic', 'zdic', 'zuic'), output=0, outputfile='finalresults/zaBPyeastppinew.txt')
enrichfuncsim('tests/nsgdBP.txt', Targets ='tests/yeastppinewbp.txt', ontology = 'BP', measure = ('wavg', 'wabm', 'wbma', 'wmax'), output=0, outputfile='finalresults/wBPyeastppinew.txt')
enrichfuncsim('tests/nsgdBP.txt', Targets ='tests/yeastppinewbp.txt', ontology = 'BP', measure = ('wgic', 'wdic', 'wuic'), output=0, outputfile='finalresults/waBPyeastppinew.txt')
enrichfuncsim('tests/nsgdBP.txt', Targets ='tests/yeastppinewbp.txt', ontology = 'BP', measure = ('uavg', 'uabm', 'ubma', 'umax'), output=0, outputfile='finalresults/uBPyeastppinew.txt')
enrichfuncsim('tests/nsgdBP.txt', Targets ='tests/yeastppinewbp.txt', ontology = 'BP', measure = ('ugic', 'udic', 'uuic', 'ui'), output=0, outputfile='finalresults/uaBPyeastppinew.txt')

from EnrichFunSim.ProteinSimilaritytw import *
enrichfuncsim('tests/nsgdBP.txt', Targets ='tests/yeastppinewbp.txt', ontology = 'BP', measure = ('teng', 'wis'), output=0, outputfile='finalresults/twBPyeastppinew.txt')

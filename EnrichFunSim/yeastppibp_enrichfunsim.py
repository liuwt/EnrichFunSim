
import sys
sys.path.append('/mnt/projects/liuwt/pathway/EnrichFunSim')
import EnrichFunSim
from EnrichFunSim import *
from EnrichFunSim.ProteinSimilarityModify import *

############################## funsim with GO-enrichment on PPIN datasets with BP, MF, CC 

enrichfuncsim('tests/nsgdBP.txt', Targets ='tests/yeastppinewbp.txt', ontology = 'BP', measure = ('ravg', 'rabm', 'rbma', 'rmax'), output=0, outputfile='finalresults/rmodifyBPyeastppinew.txt') 
enrichfuncsim('tests/nsgdBP.txt', Targets ='tests/yeastppinewbp.txt', ontology = 'BP', measure = ('xravg', 'xrabm', 'xrbma', 'xrmax'), output=0, outputfile='finalresults/xrmodifyBPyeastppinew.txt')
enrichfuncsim('tests/nsgdBP.txt', Targets ='tests/yeastppinewbp.txt', ontology = 'BP', measure = ('navg', 'nabm', 'nbma', 'nmax'), output=0, outputfile='finalresults/nmodifyBPyeastppinew.txt')
enrichfuncsim('tests/nsgdBP.txt', Targets ='tests/yeastppinewbp.txt', ontology = 'BP', measure = ('xnavg', 'xnabm', 'xnbma', 'xnmax'), output=0, outputfile='finalresults/xnmodifyBPyeastppinew.txt')
enrichfuncsim('tests/nsgdBP.txt', Targets ='tests/yeastppinewbp.txt', ontology = 'BP', measure = ('lavg', 'labm', 'lbma', 'lmax'), output=0, outputfile='finalresults/lmodifyBPyeastppinew.txt')
enrichfuncsim('tests/nsgdBP.txt', Targets ='tests/yeastppinewbp.txt', ontology = 'BP', measure = ('xlavg', 'xlabm', 'xlbma', 'xlmax'), output=0, outputfile='finalresults/xlmodifyBPyeastppinew.txt')
enrichfuncsim('tests/nsgdBP.txt', Targets ='tests/yeastppinewbp.txt', ontology = 'BP', measure = ('savg', 'sabm', 'sbma', 'smax'), output=0, outputfile='finalresults/smodifyBPyeastppinew.txt')
enrichfuncsim('tests/nsgdBP.txt', Targets ='tests/yeastppinewbp.txt', ontology = 'BP', measure = ('agic', 'adic', 'auic'), output=0, outputfile='finalresults/amodifyBPyeastppinew.txt')
enrichfuncsim('tests/nsgdBP.txt', Targets ='tests/yeastppinewbp.txt', ontology = 'BP', measure = ('zavg', 'zabm', 'zbma', 'zmax'), output=0, outputfile='finalresults/zmodifyBPyeastppinew.txt')
enrichfuncsim('tests/nsgdBP.txt', Targets ='tests/yeastppinewbp.txt', ontology = 'BP', measure = ('zgic', 'zdic', 'zuic'), output=0, outputfile='finalresults/zamodifyBPyeastppinew.txt')
enrichfuncsim('tests/nsgdBP.txt', Targets ='tests/yeastppinewbp.txt', ontology = 'BP', measure = ('wavg', 'wabm', 'wbma', 'wmax'), output=0, outputfile='finalresults/wmodifyBPyeastppinew.txt')
enrichfuncsim('tests/nsgdBP.txt', Targets ='tests/yeastppinewbp.txt', ontology = 'BP', measure = ('wgic', 'wdic', 'wuic'), output=0, outputfile='finalresults/wamodifyBPyeastppinew.txt')
enrichfuncsim('tests/nsgdBP.txt', Targets ='tests/yeastppinewbp.txt', ontology = 'BP', measure = ('uavg', 'uabm', 'ubma', 'umax'), output=0, outputfile='finalresults/umodifyBPyeastppinew.txt')
enrichfuncsim('tests/nsgdBP.txt', Targets ='tests/yeastppinewbp.txt', ontology = 'BP', measure = ('ugic', 'udic', 'uuic', 'ui'), output=0, outputfile='finalresults/uamodifyBPyeastppinew.txt')

from EnrichFunSim.ProteinSimilaritytwModify import *
enrichfuncsim('tests/nsgdBP.txt', Targets ='tests/yeastppinewbp.txt', ontology = 'BP', measure = ('teng', 'wis'), output=0, outputfile='finalresults/twmodifyBPyeastppinew.txt')